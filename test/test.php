<?php

namespace test;

use RuntimeException;
use TestInterop\Common\AssertionResult;
use TestInterop\Common\CompositeTestListener;
use TestInterop\Common\NullTestListener;
use TestInterop\TestCaseListener;
use TestInterop\TestListener;
use TestInterop\TestSuiteListener;
use Throwable;

require_once dirname(__DIR__) . "/vendor/autoload.php";

/**
 * @var int $passed number of passed tests
 * @var int $failed number of failed tests
 */

$passed = 0;
$failed = 0;

/**
 * Checks the result of an assertion and counts it as passed or failed.
 *
 * @param mixed       $value
 * @param mixed       $expected
 * @param string|null $why
 */
function check($value, $expected, ?string $why = null)
{
    global $passed, $failed;

    if ($value === $expected) {
        $passed += 1;
    } else {
        $failed += 1;

        echo "Unexpected value: " . print_r($value, true) . "\n"
            . "Expected: " . print_r($expected, true)
            . ($why ? "\nReason: {$why}" : "") . "\n";
    }
}

/**
 * Mock Test Listener implementation that records every message in public properties.
 */
class MockTestListener implements TestListener
{
    public $suites = [];

    public function beginTestSuite(string $name, array $properties = []): TestSuiteListener
    {
        return $this->suites[$name] = new class ($properties) implements TestSuiteListener
        {
            public $properties;
            public $cases = [];

            public function __construct($properties)
            {
                $this->properties = $properties;
            }

            public function beginTestCase(string $name, ?string $className = null): TestCaseListener
            {
                return $this->cases[$name] = new class ($className) implements TestCaseListener
                {
                    public $className;
                    public $results = [];
                    public $error;
                    public $reason_skipped;
                    public $reason_disabled;
                    public $ended = false;

                    public function __construct($className)
                    {
                        $this->className = $className;
                    }

                    public function addResult(\TestInterop\AssertionResult $result): void
                    {
                        $this->results[] = $result;
                    }

                    public function addError(Throwable $error): void
                    {
                        $this->error = $error;
                    }

                    public function setSkipped(string $reason): void
                    {
                        $this->reason_skipped = $reason;
                    }

                    public function setDisabled(string $reason): void
                    {
                        $this->reason_disabled = $reason;
                    }

                    public function end(): void
                    {
                        $this->ended = true;
                    }
                };
            }
        };
    }
}

/**
 * Fake a number of Test Suites with Test Cases and Assertion Results.
 *
 * @param TestListener $listener
 */
function fake_test(TestListener $listener)
{
    $suite_1 = $listener->beginTestSuite("suite 1", ["a" => 1, "b" => 2]);

    $case_1 = $suite_1->beginTestCase("case 1", "ClassName");

    $result_1 = new AssertionResult(true, "type 1");

    $case_1->addResult($result_1);

    $result_2 = new AssertionResult(false, "type 2");

    $case_1->addResult($result_2);

    $case_1->end();

    $case_2 = $suite_1->beginTestCase("case 2", null);

    $case_2->setDisabled("disabled");

    $case_2->end();

    $suite_2 = $listener->beginTestSuite("suite 2");

    $case_3 = $suite_2->beginTestCase("case 3");

    $case_3->setSkipped("skipped");

    $case_3->end();

    $case_4 = $suite_2->beginTestCase("case 4");

    $case_4->addError(new RuntimeException("error"));

    $case_4->end();
}

/**
 * Unit test for the common Assertion Result implementation
 */
function test_common_assertion()
{
    $result = new AssertionResult(true, "type 1");

    check($result instanceof \TestInterop\AssertionResult, true);
    check($result->getResult(), true);
    check($result->getType(), "type 1");

    check($result->hasValue(), false);
    $result->setValue(null);
    check($result->hasValue(), true, "null is considered a value");
    $result->setValue(123);
    check($result->getValue(), 123, "value is mutable");
    check($result->hasValue(), true);

    check($result->hasExpected(), false);
    $result->setExpected(null);
    check($result->hasExpected(), true, "null is considered an expected value");
    $result->setExpected("123");
    check($result->getExpected(), "123", "expected value is mutable");
    check($result->hasExpected(), true);

    check($result->getContext(), [], "context defaults to an empty array");
    $result->addContext(["a" => 1, "b" => 2]);
    $result->addContext(["b" => 3, "c" => 4]);

    check($result->getContext()["a"], 1, "preserves context after adding more");
    check($result->getContext()["b"], 3, "can overwrite context values");
    check($result->getContext()["c"], 4, "can add context values");

    check($result->getFile(), null);
    $result->setFile("file_1");
    check($result->getFile(), "file_1");

    check($result->getLine(), null);
    $result->setLine(123);
    check($result->getLine(), 123);

    check($result->getMessage(), null);
    $result->setMessage("message 1");
    check($result->getMessage(), "message 1");
}

/**
 * The Null Test Listener has no behavior - it merely
 * has to execute without error to pass.
 */
function test_null_listener()
{
    fake_test(new NullTestListener());
}

/**
 * Composite Test Listener should pass the same assertions for two
 * mock Test Listeners
 */
function test_composite_listener()
{
    /**
     * @var MockTestListener[] $listeners
     */

    $listeners = [
        new MockTestListener(),
        new MockTestListener(),
    ];

    fake_test(new CompositeTestListener($listeners));

    foreach ($listeners as $listener) {
        check(count($listener->suites), 2);

        check($listener->suites["suite 1"]->properties, ["a" => 1, "b" => 2]);
        check($listener->suites["suite 1"]->cases["case 1"]->className, "ClassName");
        check(count($listener->suites["suite 1"]->cases["case 1"]->results), 2);

        check($listener->suites["suite 1"]->cases["case 1"]->results[0]->getResult(), true);
        check($listener->suites["suite 1"]->cases["case 1"]->results[0]->getType(), "type 1");

        check($listener->suites["suite 1"]->cases["case 1"]->results[1]->getResult(), false);
        check($listener->suites["suite 1"]->cases["case 1"]->results[1]->getType(), "type 2");

        check($listener->suites["suite 1"]->cases["case 1"]->ended, true);

        check($listener->suites["suite 1"]->cases["case 2"]->reason_disabled, "disabled");
        check($listener->suites["suite 1"]->cases["case 2"]->ended, true);

        check($listener->suites["suite 2"]->cases["case 3"]->reason_skipped, "skipped");
        check($listener->suites["suite 2"]->cases["case 3"]->ended, true);

        check($listener->suites["suite 2"]->cases["case 4"]->error->getMessage(), "error");
        check($listener->suites["suite 2"]->cases["case 4"]->ended, true);
    }
}

// Run the tests:

test_common_assertion();
test_null_listener();
test_composite_listener();

echo "{$passed} passed, {$failed} failed tests.\n";

exit($failed > 0 ? 1 : 0);
