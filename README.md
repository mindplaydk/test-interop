Test Interop
============

These interfaces define an interoperability model for the back-end of a test-framework.

The model is designed with a streaming approach, which permits one or several
test-listeners to respond to events submitted by the front-end of a test-framework,
in chronological order, as they occur.

Examples of potential test-listeners include on-the-fly pretty printers, report
generators and aggregators, and so on.

#### Definitions

The model uses the following semantics and terminology:

  * **test-suite:** a self-contained suite of test-cases, often described as either
    "unit", "integration" or "functional", etc. - the implementation of which might
    be (for example) a set of classes, scripts or functions.
    
  * **test-case:** a test-case that performs assertions, usually described with a
    human-readable name of some sort - the implementation of which might be (for
    example) a method or function.
    
  * **result:** refers to the result of an *assertion*, e.g. *passed* or *failed*,
    along with a description of the type of assertion that was performed.
  
  * **error:** refers to an unexpected error that occurred while trying to run
    the test - for example an unhandled exception, or an error in the test itself.
    (assertion failures are not errors, as these are normal during failed tests.)

#### Function

A `TestListener` implementation produces a `TestSuiteListener` implementation for
every logical test-suite.

The `TestSuiteListener` implementation produces a `TestCaseListener` implementation,
to which assertion results can be added, errors can be recorded, or the test-case
may be flagged as either being skipped for some reason, or disabled entirely.

Messages MUST be dispatched in chronological order - that is, the test-listener
expects one test-suite to receive all test-cases, and all test-cases to receive
all assertion-results, before proceeding to the next test-suite or test-case.

#### Integration

A test-framework provides support for one or (usually) multiple implementations
of `TestListener`, and MUST dispatch messages in chronological order, to all
listeners, by calling the relevant methods.

A test-framework SHOULD dispatch messages as they occur, e.g. as opposed to
buffering the messages and dispatching them after running the tests.

#### Implementation

A test-listener receives messages from a test-framework - its function is very
loosely defined; for example, it might buffer the messages, record timestamps
and clock durations, count passed/failed assertions, report errors, and either
stream the information to a file or output, buffer the information and produce
a report, aggregate the information for statistical purposes, and so on.

If a test-listener receives a message for a prior test-suite or test-case, it
SHOULD throw a runtime-exception.

## Meta

#### Scope

The model defines only the necessary message structure for a test-framework to
communicate to a back-end facility. Specifically, any implementation details
of the front-end of the test-framework, such as the kind of assertions that
are available, are beyond the scope of this model. Any implementation details
of the back-end, such as how it counts or reports assertions or errors, are
similarly out of scope.

#### References

The model references the [JUnit version 5 Schema](https://github.com/junit-team/junit5/blob/master/platform-tests/src/test/resources/jenkins-junit.xsd)
for semantics and terminology.

The JUnit XML report format prevails as the practically universally-supported and
de-facto standard for all major test-frameworks and continuous integration tools
on all major platforms.

The TAP format was also considered for it's simplicity and streaming capability -
however, the defined semantics of TAP weren't rich enough to provide a reference an
interoperability model. TAP gets around this by permitting YAML meta-data with no
defined semantics, and this interoperability model maps to that just fine - which
means a TAP-emitting listener could be implemented.
