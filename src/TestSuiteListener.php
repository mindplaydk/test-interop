<?php

namespace TestInterop;

interface TestSuiteListener
{
    /**
     * Indicates the beginning of a new test-case.
     *
     * Note that no further messages to a previous test-case are expected - attempting
     * to do so may trigger a run-time exception.
     *
     * @param string      $name      logical name of the test-case - for example, the method-name
     *                               of a class that implements the test-case, or a name explicitly
     *                               supplied by the test-case itself.
     * @param string|null $className optional name of a class that implements the test-case.
     *
     * @return TestCaseListener
     */
    public function beginTestCase(string $name, ?string $className = null): TestCaseListener;
}
