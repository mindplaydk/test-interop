<?php

namespace TestInterop;

use Throwable;

interface TestCaseListener
{
    /**
     * Add the result of a performed assertion to the test-case.
     *
     * @param AssertionResult $result
     */
    public function addResult(AssertionResult $result): void;

    /**
     * Record an unexpected error that occurred while trying to run the test.
     *
     * @param Throwable $error the error that caused the test to break.
     */
    public function addError(Throwable $error): void;

    /**
     * Indicates that the test-case was skipped.
     *
     * For example, the test-case may be unable to run on the current platform,
     * or in the current environment, etc.
     *
     * @param string $reason
     */
    public function setSkipped(string $reason): void;

    /**
     * Flags the test-case as disabled.
     *
     * For example, the test-case may have been explicitly marked as disabled,
     * or the user may have manually disabled it on the command-line, etc.
     *
     * @param string $reason
     */
    public function setDisabled(string $reason): void;

    /**
     * Record the end of the test-case.
     *
     * This may be used by the listener to clock the duration of the test-case.
     *
     * No further messages are expected after calling this method.
     */
    public function end(): void;
}
