<?php

namespace TestInterop;

interface AssertionResult
{
    /**
     * @return bool the result of the assertion: true = passed, false = failed.
     */
    public function getResult(): bool;

    /**
     * @return string describes the type of assertion that was performed.
     */
    public function getType(): string;

    /**
     * @return string|null provides a human-readable message for the specific assertion.
     */
    public function getMessage(): ?string;

    /**
     * @return bool indicates whether an assertion value is available or not.
     *
     * @see getValue()
     */
    public function hasValue(): bool;

    /**
     * @return mixed the actual value to which the assertion was applied.
     *
     * @see hasValue()
     */
    public function getValue();

    /**
     * @return bool indicates whether an expected assertion value is available or not.
     *
     * @see getExpected()
     */
    public function hasExpected(): bool;

    /**
     * @return mixed the expected value against which a comparison was performed.
     *
     * @see hasExpected()
     */
    public function getExpected();

    /**
     * @return string|null absolute path to the source-file that implements the test-case, if available.
     */
    public function getFile(): ?string;

    /**
     * @return int|null base-1 line-number of the assertion in the source-file that implements the test-case, if available.
     */
    public function getLine(): ?int;

    /**
     * @return array map of additional context-values for the assertion.
     */
    public function getContext(): array;
}
