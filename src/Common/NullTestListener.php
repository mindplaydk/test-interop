<?php

namespace TestInterop\Common;

use TestInterop\AssertionResult;
use TestInterop\TestCaseListener;
use TestInterop\TestListener;
use TestInterop\TestSuiteListener;
use Throwable;

/**
 * A "null object" implementation of the Test Listener API.
 *
 * Useful as an internal default for optional Test Listener dependencies.
 */
class NullTestListener implements TestListener
{
    public function beginTestSuite(string $name, array $properties = []): TestSuiteListener
    {
        return new class implements TestSuiteListener
        {
            public function beginTestCase(string $name, ?string $className = null): TestCaseListener
            {
                return new class implements TestCaseListener
                {
                    public function addResult(AssertionResult $result): void
                    {
                        // EMPTY
                    }

                    public function addError(Throwable $error): void
                    {
                        // EMPTY
                    }

                    public function setSkipped(string $reason): void
                    {
                        // EMPTY
                    }

                    public function setDisabled(string $reason): void
                    {
                        // EMPTY
                    }

                    public function end(): void
                    {
                        // EMPTY
                    }
                };
            }
        };
    }
}
