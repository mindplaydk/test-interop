<?php

namespace TestInterop\Common;

use TestInterop\AssertionResult;
use TestInterop\TestCaseListener;
use TestInterop\TestListener;
use TestInterop\TestSuiteListener;
use Throwable;

/**
 * This implementation composes a series of Test Listeners.
 *
 * Useful as an internal component wrapping zero or more Test Listener dependencies.
 */
class CompositeTestListener implements TestListener
{
    /**
     * @var TestListener[]
     */
    private $test_listeners;

    /**
     * @param TestListener[] $test_listeners
     */
    public function __construct(array $test_listeners)
    {
        $this->test_listeners = $test_listeners;
    }

    public function beginTestSuite(string $name, array $properties = []): TestSuiteListener
    {
        /**
         * @var TestSuiteListener[] $test_suite_listeners
         */

        $test_suite_listeners = [];

        foreach ($this->test_listeners as $test_listener) {
            $test_suite_listeners[] = $test_listener->beginTestSuite($name, $properties);
        }

        return new class ($test_suite_listeners) implements TestSuiteListener
        {
            /**
             * @var array|TestSuiteListener[]
             */
            private $test_suite_listeners;

            /**
             * @param TestSuiteListener[] $test_suite_listeners
             */
            public function __construct(array $test_suite_listeners)
            {
                $this->test_suite_listeners = $test_suite_listeners;
            }

            public function beginTestCase(string $name, ?string $className = null): TestCaseListener
            {
                /**
                 * @var TestCaseListener[] $test_case_listeners
                 */

                $test_case_listeners = [];

                foreach ($this->test_suite_listeners as $test_suite_listener) {
                    $test_case_listeners[] = $test_suite_listener->beginTestCase($name, $className);
                }

                return new class ($test_case_listeners) implements TestCaseListener
                {
                    /**
                     * @var TestCaseListener[]
                     */
                    private $test_case_listeners;

                    /**
                     * @param TestCaseListener[] $test_case_listeners
                     */
                    public function __construct($test_case_listeners)
                    {
                        $this->test_case_listeners = $test_case_listeners;
                    }

                    public function addResult(AssertionResult $result): void
                    {
                        foreach ($this->test_case_listeners as $test_case_listener) {
                            $test_case_listener->addResult($result);
                        }
                    }

                    public function addError(Throwable $error): void
                    {
                        foreach ($this->test_case_listeners as $test_case_listener) {
                            $test_case_listener->addError($error);
                        }
                    }

                    public function setSkipped(string $reason): void
                    {
                        foreach ($this->test_case_listeners as $test_case_listener) {
                            $test_case_listener->setSkipped($reason);
                        }
                    }

                    public function setDisabled(string $reason): void
                    {
                        foreach ($this->test_case_listeners as $test_case_listener) {
                            $test_case_listener->setDisabled($reason);
                        }
                    }

                    public function end(): void
                    {
                        foreach ($this->test_case_listeners as $test_case_listener) {
                            $test_case_listener->end();
                        }
                    }
                };
            }
        };
    }
}
