<?php

namespace TestInterop\Common;

/**
 * Common implementation of an Assertion Result.
 *
 * This can be optionally used as a base-class or placeholder, mock, etc.
 */
class AssertionResult implements \TestInterop\AssertionResult
{
    /**
     * @var bool
     */
    protected $result;

    /**
     * @var string
     */
    protected $type;

    /**
     * @var string|null
     */
    protected $message = null;

    /**
     * @var bool
     */
    protected $has_value = false;

    /**
     * @var mixed
     */
    protected $value = null;

    /**
     * @var bool
     */
    protected $has_expected = false;

    /**
     * @var mixed
     */
    protected $expected = null;

    /**
     * @var string|null
     */
    protected $file = null;

    /**
     * @var int|null
     */
    protected $line = null;

    /**
     * @var array
     */
    protected $context = [];

    /**
     * @param bool   $result
     * @param string $type
     */
    public function __construct(bool $result, string $type)
    {
        $this->result = $result;
        $this->type = $type;
    }

    public function getResult(): bool
    {
        return $this->result;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(?string $message): void
    {
        $this->message = $message;
    }

    public function hasValue(): bool
    {
        return $this->has_value;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function setValue($value): void
    {
        $this->value = $value;

        $this->has_value = true;
    }

    public function hasExpected(): bool
    {
        return $this->has_expected;
    }

    public function getExpected()
    {
        return $this->expected;
    }

    public function setExpected($expected): void
    {
        $this->expected = $expected;

        $this->has_expected = true;
    }

    public function getFile(): ?string
    {
        return $this->file;
    }

    public function setFile(?string $file): void
    {
        $this->file = $file;
    }

    public function getLine(): ?int
    {
        return $this->line;
    }

    public function setLine(?int $line): void
    {
        $this->line = $line;
    }

    public function getContext(): array
    {
        return $this->context;
    }

    public function addContext(array $context): void
    {
        $this->context = $context + $this->context;
    }
}
