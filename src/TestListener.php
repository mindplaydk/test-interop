<?php

namespace TestInterop;

interface TestListener
{
    /**
     * Indicates the beginning of a new test-suite.
     *
     * Note that no further messages to a previous test-suite are expected - attempting
     * to do so may trigger a run-time exception.
     *
     * @param string $name       logical name of the test-suite.
     * @param array  $properties optional properties, such as environment variables
     *                           or test-suite configuration settings.
     *
     * @return TestSuiteListener
     */
    public function beginTestSuite(string $name, array $properties = []): TestSuiteListener;
}
